exports.config = {  
    seleniumAddress: 'http://localhost:4444/wd/hub',  
    specs: ['./features/*.feature'],  
    baseURL: 'http://localhost:3000',  
    framework: 'custom',  
	frameworkPath: require.resolve('protractor-cucumber-framework/'),
	capabilities: {
		browserName: 'chrome'
	},
	cucumberOpts: {
		format:  'node_modules/cucumber-pretty',
		tags: false,
		require: './features/step_definitions/*.js',
		profile: false,
		'no-source': true
	},
	params: {
		env: {
		  hostname: 'http://localhost:3000'
		}
	}
};