var chai               = require('chai'),
    chaiAsPromised     = require('chai-as-promised'),
    expect             = chai.expect;

chai.use(chaiAsPromised);

var HomePO = require('../../pageObjects/checkWriter.po.js'),
    home   = new HomePO();

var {defineSupportCode} = require('cucumber');

defineSupportCode(function ({Given, When, Then}) {

  Given('I view the homepage', function(callback) {
    browser
      .get(browser.params.env.hostname + "/check-writer")
      .then(callback);
  });
  
  When('I enter currency value in the text box', function (callback) {
    home
    .enterCurrencyValue('12945.78')
    .then(callback);
  });
  
  When('I click on the Submit button', function (callback) {
    home
    .clickSubmitButton()
    .then(callback);
  });
  
  Then('I should see the English equivalent of the entered currency value.', function (callback) {
  expect(home.isResultPresent())
    .to.eventually.equal(true)
    .and.notify(callback);
});

});