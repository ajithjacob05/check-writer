Feature: CheckWriter
  Scenario: Convert currency
    Given I view the homepage
	When I enter currency value in the text box
    And I click on the Submit button
    Then I should see the English equivalent of the entered currency value.