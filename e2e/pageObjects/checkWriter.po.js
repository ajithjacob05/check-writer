var HomepagePageObject = function() {
  this.selectors = {
	'CURRENCY_TEXT_BOX': '.e2e-currency-text-box',
    'SUBMIT_BUTTON': '.e2e-submit-button',
    'RESULT'      : '.e2e-result'
  };

  this.currencyTextBox = browser.$(this.selectors.CURRENCY_TEXT_BOX);
  this.submitButton = browser.$(this.selectors.SUBMIT_BUTTON);
  this.result = browser.$(this.selectors.RESULT);
};

HomepagePageObject.prototype.clickSubmitButton = function() {
  return this.submitButton.click();
};

HomepagePageObject.prototype.isResultPresent = function() {
  return this.result.isPresent();
};

HomepagePageObject.prototype.enterCurrencyValue = function(value) {
  return this.currencyTextBox.sendKeys(value);
};

module.exports = HomepagePageObject;