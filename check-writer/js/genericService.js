
(function(){
	angular.module("checkWriter")
                                                     
	.factory('genericService', ['$log', function ($log) {
		
		var wordArrays = {
			ones : ['','one','two','three','four', 'five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen'],
			tens : ['','','twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'],
			hundreds : ['hundred'],
			thousands : ['','thousand', 'million', 'billion', 'trillion']
		}
		
		// service definition
		var service = {};
		
		//service implementation
		
		
		/*
		Split the string value to the group of threes
		Example: "3456789" gives [['7', '8', '9'], ['4', '5', '6'], ['3']]
		*/		
		service.splitIntoGroups = function(value){
			var valueArray = value.split('');
			var offset = 3;
			var length = valueArray.length;
			var limit = Math.floor(length/offset);
			var groupArray = [];
			for (i = 0; i<limit; i++){
				groupArray[i] = valueArray.splice(valueArray.length - 3, valueArray.length);
			}
			if(valueArray.length > 0){
				groupArray[limit] = valueArray;
			}
			return groupArray;
			
		}
		
		service.getCentsFromDecPart = function(decimalPart){
			var decPart = "." + decimalPart;
			var parse = parseFloat(decPart);
			if(isNaN(parse)){
				parse = 0;
			}
			var decPartUdjustedToTwoDigits = parse.toFixed(2);
			decPart = decPartUdjustedToTwoDigits.toString();
			return decPart.slice(decPart.indexOf('.') + 1);
		}
		
		/*
		Convert three digit number to words.
		Example: input - ['3', '1', '5'], output - "three hundred fifteen"
		*/
		service.convertThreeDigitNumberToWords = function(value){
			var valueNum = parseInt(value.join(""));
			var valueNumArray = valueNum.toString().split('');
			var inWords = '';
			switch(valueNum.toString().length){
				case 1:
				inWords = wordArrays.ones[valueNum];
				break;
				case 2:
				if(valueNumArray[0] == 1){
					inWords = wordArrays.ones[valueNum];
				}else{
					inWords = wordArrays.tens[parseInt(valueNumArray[0])];
					if(wordArrays.ones[parseInt(valueNumArray[1])]){
						inWords = inWords + " " + wordArrays.ones[parseInt(valueNumArray[1])];
					}
				}
				break;
				case 3:
				inWords = wordArrays.ones[parseInt(valueNumArray[0])] + " " + wordArrays.hundreds[0];
				if(valueNumArray[1] == 1){
					var num = (valueNumArray[1] + valueNumArray[2])
					inWords = inWords + " " + wordArrays.ones[parseInt(num)];
				}else{
					if(wordArrays.tens[parseInt(valueNumArray[1])]){
						inWords = inWords + " " + wordArrays.tens[parseInt(valueNumArray[1])];
					}
					if(wordArrays.ones[parseInt(valueNumArray[2])]){
						inWords = inWords + " " + wordArrays.ones[parseInt(valueNumArray[2])];
					}
				}
				break;
			}
			return inWords;
		}
		
		service.getWordFromThousands = function(param){
			return wordArrays.thousands[param];
		}
		
		//return service
		return service;
		}]);	
})();