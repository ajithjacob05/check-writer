﻿(function(){
'use strict';
angular.module("checkWriter")
.controller('CurrencyController', [ '$scope', 'currencyService', function($scope, currencyService) {
  $scope.covertCurrency = function () {
		var value = $scope.currency.value;
		$scope.currency.words = currencyService.convertCurrencyToWords(value);
		$scope.result = true;
	};
}]);
})();