(function(){
	angular.module("checkWriter")
                                                     
	.factory('currencyService', ['$log', 'genericService', function ($log, genericService) {
		
		// service definition
		var service = {};
		
		//service implementation
		service.convertCurrencyToWords = function(value){
			var currencyInWords = "";
			var values = value.split('.');
			values[0] = values[0].replace(/,/g, '');
			
			if(values[0] && values[0].length > 15){
				return "You entered a too big value!.";
			}
			var currWholePart = parseInt(values[0]);
			if(isNaN(currWholePart)){
				return "Please enter a valid currency value!.";
			}
			
			//Convert the whole number part to English equivalent.
			var wholeNumberWords = convertWholeNumber(currWholePart.toString());
			
			//gets the cents from the decimal part. Fixed to two decimal points.
			//This method can handle junk inputs and undefined. In such cases, it will return "00".
			var cents = genericService.getCentsFromDecPart(values[1]);
			
			if(parseInt(cents) === 0){
				currencyInWords = wholeNumberWords + " dollars only";
			}else{
				currencyInWords = wholeNumberWords + " dollars and " + cents + "/100";
			}
			$log.info('converted to English equivalent!');
			
			var valueInWordsArray = currencyInWords.split('');
			valueInWordsArray[0] = valueInWordsArray[0].toUpperCase();
			currencyInWords = valueInWordsArray.join('');
			return currencyInWords;
			
		}
		/*
		Convert whole number to English equivalent.
		*/
		var convertWholeNumber = function(value){
			var groupedValueArray = genericService.splitIntoGroups(value);
			
			var wholeNumberPartWords = '';
			for(i=groupedValueArray.length-1;i>=0;i--){
				var groupWords = '';
				if(i==0){
					groupWords = genericService.convertThreeDigitNumberToWords(groupedValueArray[i]);
					if(groupWords){
						if(wholeNumberPartWords){
							wholeNumberPartWords = wholeNumberPartWords + " " + groupWords;
						}else{
							wholeNumberPartWords = groupWords;
						}
					}
				}else{
					groupWords = genericService.convertThreeDigitNumberToWords(groupedValueArray[i]);
					if(groupWords){
						if(wholeNumberPartWords){
							wholeNumberPartWords = wholeNumberPartWords + " " + groupWords + " " + genericService.getWordFromThousands(i);
						}else{
							wholeNumberPartWords = groupWords + " " + genericService.getWordFromThousands(i);
						}
					}
				}
			}
			if(wholeNumberPartWords === ''){
				wholeNumberPartWords = 'Zero';
			}
			return wholeNumberPartWords;
			
		}
		
		//return service
		return service;
		}]);	
})();