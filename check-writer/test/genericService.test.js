
describe('genericService', function () {
    // define variables for the services we want to access in tests
    var genericService;
 
    beforeEach(function () {
        // load the module we want to test
        module('checkWriter');
 
        // inject the services we want to test
        inject(function (_genericService_) {
            genericService = _genericService_;
        })
    });
	
    describe('#splitIntoGroups', function () {
        it('should return an array of arrays', function () {
            // Arrange
            sinon.spy(genericService, 'splitIntoGroups');
 
            // Act
            genericService.splitIntoGroups('45623');
     
            // Assert
            assert(genericService.splitIntoGroups.returned([['6', '2', '3'], ['4', '5']]));
 
            // Cleanup
            genericService.splitIntoGroups.restore();
        });
		
    });
	
	describe('#convertThreeDigitNumberToWords', function () {
        it('should return the English equivalent of three digit number : 456', function () {
            // Arrange
            sinon.spy(genericService, 'convertThreeDigitNumberToWords');
 
            // Act
            genericService.convertThreeDigitNumberToWords(['4', '5', '6']);
     
            // Assert
            assert(genericService.convertThreeDigitNumberToWords.returned('four hundred fifty six'));
 
            // Cleanup
            genericService.convertThreeDigitNumberToWords.restore();
        });
		
    });
	
	describe('#convertThreeDigitNumberToWords', function () {
        it('should return the English equivalent of three digit number : 515', function () {
            // Arrange
            sinon.spy(genericService, 'convertThreeDigitNumberToWords');
 
            // Act
            genericService.convertThreeDigitNumberToWords(['5', '1', '5']);
     
            // Assert
            assert(genericService.convertThreeDigitNumberToWords.returned('five hundred fifteen'));
 
            // Cleanup
            genericService.convertThreeDigitNumberToWords.restore();
        });
		
    });
	
	describe('#getCentsFromDecPart', function () {
        it('should return the cents fixed to 2', function () {
            // Arrange
            sinon.spy(genericService, 'getCentsFromDecPart');
 
            // Act
            genericService.getCentsFromDecPart('');
     
            // Assert
            assert(genericService.getCentsFromDecPart.returned('00'));
 
            // Cleanup
            genericService.getCentsFromDecPart.restore();
        });
		
		it('should return the cents fixed to 2', function () {
            // Arrange
            sinon.spy(genericService, 'getCentsFromDecPart');
 
            // Act
            genericService.getCentsFromDecPart("4hfcc88");
     
            // Assert
            assert(genericService.getCentsFromDecPart.returned('40'));
 
            // Cleanup
            genericService.getCentsFromDecPart.restore();
        });
		
		it('should return the cents fixed to 2', function () {
            // Arrange
            sinon.spy(genericService, 'getCentsFromDecPart');
 
            // Act
            genericService.getCentsFromDecPart(undefined);
     
            // Assert
            assert(genericService.getCentsFromDecPart.returned('00'));
 
            // Cleanup
            genericService.getCentsFromDecPart.restore();
        });
		
		it('should return the cents fixed to 2', function () {
            // Arrange
            sinon.spy(genericService, 'getCentsFromDecPart');
 
            // Act
            genericService.getCentsFromDecPart(',,,,,');
     
            // Assert
            assert(genericService.getCentsFromDecPart.returned('00'));
 
            // Cleanup
            genericService.getCentsFromDecPart.restore();
        });
		
    });
	
});