
describe('currencyService', function () {
    // define variables for the services we want to access in tests
    var currencyService,
        $log;
 
    beforeEach(function () {
        // load the module we want to test
        module('checkWriter');
 
        // inject the services we want to test
        inject(function (_currencyService_, _$log_) {
            currencyService = _currencyService_;
            $log = _$log_;
        })
    });
	
    describe('#convertCurrencyToWords', function () {
        it('should log the message "converted to English equivalent!"', function () {
            // Arrange
            sinon.spy($log, 'info');
 
            // Act
            currencyService.convertCurrencyToWords('123.45');
     
            // Assert
            assert($log.info.calledOnce);
            assert($log.info.calledWith('converted to English equivalent!'));
 
            // Cleanup
            $log.info.restore();
        });
		
		it('should print English equivalent of : 5', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('5');
			
			assert(currencyService.convertCurrencyToWords.returned('Five dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 50', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('50');
			
			assert(currencyService.convertCurrencyToWords.returned('Fifty dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 50.00', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('50.00');
			
			assert(currencyService.convertCurrencyToWords.returned('Fifty dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 500', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('500');
			
			assert(currencyService.convertCurrencyToWords.returned('Five hundred dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 520', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('520');
			
			assert(currencyService.convertCurrencyToWords.returned('Five hundred twenty dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 525', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('525');
			
			assert(currencyService.convertCurrencyToWords.returned('Five hundred twenty five dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('516');
			
			assert(currencyService.convertCurrencyToWords.returned('Five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 516.75', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('516.75');
			
			assert(currencyService.convertCurrencyToWords.returned('Five hundred sixteen dollars and 75/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 5516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('5516');
			
			assert(currencyService.convertCurrencyToWords.returned('Five thousand five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 65516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('65516');
			
			assert(currencyService.convertCurrencyToWords.returned('Sixty five thousand five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 865516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('865516');
			
			assert(currencyService.convertCurrencyToWords.returned('Eight hundred sixty five thousand five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 4865516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('4865516');
			
			assert(currencyService.convertCurrencyToWords.returned('Four million eight hundred sixty five thousand five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 6524865516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('6524865516');
			
			assert(currencyService.convertCurrencyToWords.returned('Six billion five hundred twenty four million eight hundred sixty five thousand five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 2346524865516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('2346524865516');
			
			assert(currencyService.convertCurrencyToWords.returned('Two trillion three hundred forty six billion five hundred twenty four million eight hundred sixty five thousand five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 562346524865516', function () {
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('562346524865516');
			
			assert(currencyService.convertCurrencyToWords.returned('Five hundred sixty two trillion three hundred forty six billion five hundred twenty four million eight hundred sixty five thousand five hundred sixteen dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 562346524865516.78', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('562346524865516.78');
			
			assert(currencyService.convertCurrencyToWords.returned('Five hundred sixty two trillion three hundred forty six billion five hundred twenty four million eight hundred sixty five thousand five hundred sixteen dollars and 78/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 7', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('7');
			
			assert(currencyService.convertCurrencyToWords.returned('Seven dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 007.00', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('007.00');
			
			assert(currencyService.convertCurrencyToWords.returned('Seven dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 7.8...6', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('7.8...6');
			
			assert(currencyService.convertCurrencyToWords.returned('Seven dollars and 80/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 7.8ewfhuie6', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('7.8ewfhuie6');
			
			assert(currencyService.convertCurrencyToWords.returned('Seven dollars and 80/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 7.8456789', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('7.8456789');
			
			assert(currencyService.convertCurrencyToWords.returned('Seven dollars and 85/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 5000', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('5000');
			
			assert(currencyService.convertCurrencyToWords.returned('Five thousand dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 5007', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('5007');
			
			assert(currencyService.convertCurrencyToWords.returned('Five thousand seven dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 6000000000007', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('6000000000007');
			
			assert(currencyService.convertCurrencyToWords.returned('Six trillion seven dollars only'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 000.67', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('000.67');
			
			assert(currencyService.convertCurrencyToWords.returned('Zero dollars and 67/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 6gjj.67', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('6gjj.67');
			
			assert(currencyService.convertCurrencyToWords.returned('Six dollars and 67/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : 6,,00.67', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('6,,00.67');
			
			assert(currencyService.convertCurrencyToWords.returned('Six hundred dollars and 67/100'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
		it('should print English equivalent of : gfh600.67', function () {
			
            // Arrange
            sinon.spy(currencyService, 'convertCurrencyToWords');
 
            // Act
            currencyService.convertCurrencyToWords('gfh600.67');
			
			assert(currencyService.convertCurrencyToWords.returned('Please enter a valid currency value!.'));
 
            // Cleanup
            currencyService.convertCurrencyToWords.restore();
        });
		
    });
});