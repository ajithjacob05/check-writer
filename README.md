
## Install project dependencies

yarn or yarn install

---

## Run Application

yarn start

---

## Unit Test

Open separate terminal and run

yarn test

---

## E2e Testing

Open separate terminal and run below commands in order

yarn update-webdriver

yarn start-webdriver

Open another terminal and run

yarn protractor

## Notes

UI validations not done. So please enter valid currencies to see the result.

It can handle any big value, but currently limiting to fifteen digits. It can grow to any level by adding more items to the 'thausands' array in generic service.

---