// Karma configuration
// Generated on Tue Mar 06 2018 02:03:48 GMT-0800 (Pacific Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: 'check-writer',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'chai', 'sinon'],


    // list of files / patterns to load in the browser
    files: [
	  '../node_modules/angular/angular.min.js',
      '../node_modules/angular-mocks/angular-mocks.js',
	  '../node_modules/angular-route/angular-route.min.js',
	  '../node_modules/angular-animate/angular-animate.min.js',
	  '../node_modules/angular-aria/angular-aria.min.js',
	  '../node_modules/angular-messages/angular-messages.min.js',
	  '../node_modules/angular-material/angular-material.min.js',
      'js/*.js',
      //'test/**/*spec.js',
	  'test/*.js',
	  //'js/app.js',

      '**/*.js'
    ],

	client: {
      mocha: {
        // change Karma's debug.html to the mocha web reporter 
        reporter: 'html',
		
		// custom ui, defined in required file above 
        ui: 'bdd-lazy-var/global',
 
        // require specific files after Mocha is initialized 
        require: [require.resolve('bdd-lazy-var/global')]
 
      }
    },

    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: [//'progress', 
	'spec', 'html'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
